package os_project;

import java.util.LinkedList;

public class AlgoFCFS {
	private static Process running_Q;
	private static LinkedList<Process> ready_Q = new LinkedList<Process>();
	private static LinkedList<Process> ready$suspend_Q = new LinkedList<Process>();
	private static LinkedList<Process> blocked_Q = new LinkedList<Process>();
	private static LinkedList<Process> blocked$suspend_Q = new LinkedList<Process>();
	private static LinkedList<Process> terminated_Q = new LinkedList<Process>();
	

	public static void addProcess(int ID, int state, String startTime, int startTimeSec, String terminationTime,int cpuTime1, 
														int cpuTime2, int size, int timeInIO, String ioType, int mode){
		Process tmp = new Process( ID, state, startTime, startTimeSec, terminationTime, cpuTime1, cpuTime2, size, timeInIO, ioType, mode);
		//.add would append the process to the end of the list 
		switch(state){
		case Process.readyState:
			ready_Q.add(tmp);
			break;
		case Process.readySuspendState:
			ready$suspend_Q.add(tmp);
			break;
		case Process.blockedState:
			blocked_Q.add(tmp);
			break;
		case Process.blockedSuspendState:
			blocked$suspend_Q.add(tmp);
			break;
		case Process.terminationState:
			terminated_Q.add(tmp);
		}
	}
	
	public static void addProcess(Process p, int state){
		Process tmp = new Process(p, state);
		//.add would append the process to the end of the list 
		switch(state){
		case Process.readyState:
			ready_Q.add(tmp);
		break;
		case Process.readySuspendState:
			ready$suspend_Q.add(tmp);
		break;
		case Process.blockedState:
			blocked_Q.add(tmp);
		break;
		case Process.blockedSuspendState:
			blocked$suspend_Q.add(tmp);
		break;
		case Process.terminationState:
			terminated_Q.add(tmp);
		}
	}
	public static int getNbRunning(){
		if (running_Q == null)
			return 0;
		return 1;
	}
	public static int getNbReady(){
		return ready_Q.size();
	}
	public static int getNbTerminated(){
		return terminated_Q.size();
	}
	public static int getNbReady$suspend(){
		return ready$suspend_Q.size();
	}
	public static int getNbBlocked(){
		return blocked_Q.size();
	}
	public static int getNbBlocked$suspend(){
		return blocked$suspend_Q.size();
	}
	// get the total number of process 
	public static int getTotalNbProcess(){
		 return (getNbRunning() + getNbReady() + getNbReady$suspend() + getNbBlocked() + getNbBlocked$suspend());
	}
	
	/*decrement io time for the blocked and blocked-suspend elements */
	public static void decrementIO(){
		for(int i = 0; i < blocked_Q.size(); i++){
			if (blocked_Q.get(i).getIo().getTimeInIO() > 0)
				blocked_Q.get(i).getIo().setTimeInIO(blocked_Q.get(i).getIo().getTimeInIO()-1); 
		}
		for(int i = 0; i < blocked$suspend_Q.size(); i++){
			if (blocked$suspend_Q.get(i).getIo().getTimeInIO() > 0)
				blocked$suspend_Q.get(i).getIo().setTimeInIO(blocked$suspend_Q.get(i).getIo().getTimeInIO()-1); 
		}
	}
	// Decrement the running process 
	public static void decrementRunning(int mode){
		if (mode == 1)
			running_Q.setCpuTime1(running_Q.getCpuTime1()-1);
		else 
			running_Q.setCpuTime2(running_Q.getCpuTime2()-1);
	}
	// get and remove the first element of ready_Q
	public static void assignRunning(){
		running_Q = new Process(ready_Q.removeFirst());
	}
	public static void clearRunning(){
		running_Q = null;
	}
	// final is to prevent any kind of change 
	public static final Process getCurrentP(){
		return running_Q;
	}
	// Fill the tabel 
	public static void fillTabel(){
		InterfaceFrame.model.getDataVector().removeAllElements();
		InterfaceFrame.model.fireTableDataChanged();
		if(getNbRunning() == 1)
			InterfaceFrame.model.addRow(new Object[]{running_Q.getID(), Process.stateHeader[running_Q.getState()], 
					running_Q.getStartTime(), running_Q.getTerminationTime(), 
					running_Q.getCpuTime1(), running_Q.getIo().getTimeInIO(), 
					running_Q.getCpuTime2(), running_Q.getMem().getSize(), 
					running_Q.getIo().getIoType()});
		for(int i = 0; i < ready_Q.size(); i++){
			InterfaceFrame.model.addRow(new Object[]{ready_Q.get(i).getID(), Process.stateHeader[ready_Q.get(i).getState()], 
					ready_Q.get(i).getStartTime(), ready_Q.get(i).getTerminationTime(), 
					ready_Q.get(i).getCpuTime1(), ready_Q.get(i).getIo().getTimeInIO(), 
					ready_Q.get(i).getCpuTime2(), ready_Q.get(i).getMem().getSize(), ready_Q.get(i).getIo().getIoType(), "--:--:--"});
		}
		for(int i = 0; i < ready$suspend_Q.size(); i++){
			InterfaceFrame.model.addRow(new Object[]{ready$suspend_Q.get(i).getID(), Process.stateHeader[ready$suspend_Q.get(i).getState()], 
					ready$suspend_Q.get(i).getStartTime(), ready$suspend_Q.get(i).getTerminationTime(), 
					ready$suspend_Q.get(i).getCpuTime1(), ready$suspend_Q.get(i).getIo().getTimeInIO(), 
					ready$suspend_Q.get(i).getCpuTime2(), ready$suspend_Q.get(i).getMem().getSize(), ready$suspend_Q.get(i).getIo().getIoType(), "--:--:--"});
		}
		for(int i = 0; i < blocked_Q.size(); i++){
			InterfaceFrame.model.addRow(new Object[]{blocked_Q.get(i).getID(), Process.stateHeader[blocked_Q.get(i).getState()], 
					blocked_Q.get(i).getStartTime(), blocked_Q.get(i).getTerminationTime(), 
					blocked_Q.get(i).getCpuTime1(), blocked_Q.get(i).getIo().getTimeInIO(), 
					blocked_Q.get(i).getCpuTime2(), blocked_Q.get(i).getMem().getSize(), blocked_Q.get(i).getIo().getIoType(), "--:--:--"});
		}
		for(int i = 0; i < blocked$suspend_Q.size(); i++){
			InterfaceFrame.model.addRow(new Object[]{blocked$suspend_Q.get(i).getID(), Process.stateHeader[blocked$suspend_Q.get(i).getState()], 
					blocked$suspend_Q.get(i).getStartTime(), blocked$suspend_Q.get(i).getTerminationTime(), 
					blocked$suspend_Q.get(i).getCpuTime1(), blocked$suspend_Q.get(i).getIo().getTimeInIO(), 
					blocked$suspend_Q.get(i).getCpuTime2(), blocked$suspend_Q.get(i).getMem().getSize(), blocked$suspend_Q.get(i).getIo().getIoType(), "--:--:--"});
		}
	}
	public static void RStoR(){ // Ready suspend to ready 
		// i add any presses that fits in the free space available 
		for(int i = 0; i < ready$suspend_Q.size(); i++){
			if (ready$suspend_Q.get(i).getMem().getSize()+Memory.getCurrentSize() <= Memory.getMaxMemorySize()){
				Memory.setCurrentSize(ready$suspend_Q.get(i).getMem().getSize());
				addProcess(ready$suspend_Q.remove(i), Process.readyState);
			}
		}
	}
	public static void BtoBS(){ // blocked to blocked suspend
		int dumSize = ready$suspend_Q.getFirst().getMem().getSize();
		boolean found = false;
		for(int i = 0; i < blocked_Q.size(); i++){
			if (blocked_Q.get(i).getMem().getSize() >= dumSize){
				found = true;
				Memory.setCurrentSize(-blocked_Q.get(i).getMem().getSize()); // drop the size
				addProcess(blocked_Q.remove(i), Process.blockedSuspendState);
				Memory.setCurrentSize(ready$suspend_Q.getFirst().getMem().getSize()); // drop the size
				addProcess(ready$suspend_Q.removeFirst(), Process.readyState);
				break;
			} 
		}
		
		if (!found){
			int sizeSum = 0;
			for(int i = 0; i < blocked_Q.size(); i++){
				if (sizeSum < dumSize){
					sizeSum += (blocked_Q.get(i).getMem().getSize());
					Memory.setCurrentSize(-blocked_Q.get(i).getMem().getSize()); // drop the size
					addProcess(blocked_Q.remove(i), Process.blockedSuspendState);
				}
			}
			// now after i freed the space i can add from R/S
			Memory.setCurrentSize(ready$suspend_Q.getFirst().getMem().getSize()); // drop the size
			addProcess(ready$suspend_Q.removeFirst(), Process.readyState);
		}
	}
	public static void BStoB(){ // blocked suspend to blocked 
		for(int i = 0; i < blocked$suspend_Q.size(); i++){
			if (blocked$suspend_Q.get(i).getMem().getSize()+Memory.getCurrentSize() <= Memory.getMaxMemorySize()){
				Memory.setCurrentSize(blocked$suspend_Q.get(i).getMem().getSize());
				addProcess(blocked$suspend_Q.remove(i), Process.blockedState);
			}
		}
	}
	public static void BtoR(){ // blocked to ready 
		for(int i = 0; i < blocked_Q.size(); i++){
			if (blocked_Q.get(i).getIo().getTimeInIO() == 0){
				addProcess(blocked_Q.remove(i), Process.readyState);
			} 
		}
	}
	public static void fillPerfTabel(){
		PerformanceOutput.model.getDataVector().removeAllElements();
		PerformanceOutput.model.fireTableDataChanged();
		for(int i = 0; i < terminated_Q.size(); i++){
			PerformanceOutput.model.addRow(new Object[]{terminated_Q.get(i).getID(), Process.stateHeader[terminated_Q.get(i).getState()], 
					terminated_Q.get(i).getStartTime(), terminated_Q.get(i).getTerminationTime(), 
					terminated_Q.get(i).getCpuTime1(), terminated_Q.get(i).getIo().getTimeInIO(), 
					terminated_Q.get(i).getCpuTime2(), terminated_Q.get(i).getMem().getSize(), terminated_Q.get(i).getIo().getIoType()
					,Control.TATime(terminated_Q.get(i).getTaTimeSec())
					}
			);
		}
	}

	// return the sum of the turn around time of all finished processes 
	public static long getTerminatedSum(){
		long dum = 0;
		for(int i = 0; i < terminated_Q.size(); i++){
			dum += terminated_Q.get(i).getTaTimeSec();
		}
		return dum;
	}
}// End of AlgoFCFS