/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

public class Memory { 
	private int size; // Size of the Process
	private final static int MAX_MEMORY_SIZE = 20480;  // 20 MB=20480 KB | is specified as max size 
	private static int currentSize = 0;	// the current amount of reserved memory
	
	public Memory(int size){
		this.setSize(size);
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public static int getCurrentSize() {
		return currentSize;
	}
	// Be careful that it adds
	public static void setCurrentSize(int currentSize) {
		Memory.currentSize += (currentSize);
	}
	public static int getMaxMemorySize() {
		return MAX_MEMORY_SIZE;
	}
}