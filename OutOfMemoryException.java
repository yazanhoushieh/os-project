package os_project;

import javax.swing.JOptionPane;

public class OutOfMemoryException extends Exception{
	public OutOfMemoryException (String msg){
		super(msg);
	}
	public OutOfMemoryException (int size){
		JOptionPane.showMessageDialog(null, String.format("[Requried Memory = %dKB]\n[Remaining Memory = %dKB]",
											size, Memory.getMaxMemorySize() - Memory.getCurrentSize()), "Out of Memory",JOptionPane.ERROR_MESSAGE);
	}
}
