package os_project;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PerformanceOutput extends JFrame{
	private static final long serialVersionUID = 1L;

	public static DefaultTableModel model;
	private static String [] data_T_Columns = {"ID","State" , "Start Time", "Terminat T.",
												"cpuTime1", "timeInIO", "cpuTime2", "size" , "ioType", "Time Around"};
	private static JTable data_T;
	private Dimension data_TDim;
	private Dimension frameDim;
	private Dimension compDim;
	private static JLabel algoName;
	private static JLabel nbProcess;
	private static JLabel algoAv;
	private JButton perfOutput_B;
	private Point algoName_loc;
	private Point nbProcess_loc;
	private Point algoAv_loc;
	private Point data_T_loc;
	private Point perfOutput_B_loc;
	public static int algoNb = 0;// according to the combo box 
	
	public PerformanceOutput(){
		data_TDim = new Dimension(820, 300);
		frameDim = new Dimension(835, 450);
		compDim = new Dimension(300, 30);
		algoName_loc = new Point(10, 10);
		nbProcess_loc = new Point(10, 40);
		algoAv_loc = new Point(10, 70);
		data_T_loc = new Point(0, 110);
		perfOutput_B_loc = new Point(400, 10);
		
		algoName = new JLabel(String.format("Algorithm : "));
		algoName.setLocation(algoName_loc);
		algoName.setSize((int)compDim.getWidth(), (int)compDim.getHeight());
		algoName.setForeground(Color.blue);

		nbProcess = new JLabel(String.format("nbProcess : "));
		nbProcess.setLocation(nbProcess_loc);
		nbProcess.setSize((int)compDim.getWidth(), (int)compDim.getHeight());
		nbProcess.setForeground(Color.blue);

		algoAv = new JLabel(String.format("average : "));
		algoAv.setLocation(algoAv_loc);
		algoAv.setSize((int)compDim.getWidth(), (int)compDim.getHeight());
		algoAv.setForeground(Color.blue);
		
		perfOutput_B = new JButton("Show Performance");
		perfOutput_B.setSize((int)compDim.getWidth()-120, (int)compDim.getHeight());
		perfOutput_B.setLocation(perfOutput_B_loc);
		perfOutput_B.addMouseListener(
				new MouseAdapter() {
					public void mouseEntered(MouseEvent e) {
						perfOutput_B.setBackground(Color.white);
					}
					public void mouseExited(MouseEvent e) {
						perfOutput_B.setBackground(getBackground());
					}
				}
		);
		perfOutput_B.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					switch(algoNb){
					case 0: // round robin
						AlgoRoundRobin.fillPerfTabel();
						algoName.setText("Algorithm : Round Robin");
						nbProcess.setText("nbProcess : " + AlgoRoundRobin.getNbTerminated());
						algoAv.setText("average : "+ ((AlgoRoundRobin.getTerminatedSum()*1.0) / AlgoRoundRobin.getNbTerminated()));
						break;
					case 1:// fcfs
						AlgoFCFS.fillPerfTabel();
						algoName.setText("Algorithm : FCFS");
						nbProcess.setText("nbProcess : " + AlgoFCFS.getNbTerminated());
						algoAv.setText("average : "+ (AlgoFCFS.getTerminatedSum()*1.0/AlgoFCFS.getNbTerminated()));
						break;
					case 2:// spn
						AlgoSPN.fillPerfTabel();
						algoName.setText("Algorithm : SPN");
						nbProcess.setText("nbProcess : " + AlgoSPN.getNbTerminated());
						algoAv.setText("average : "+ (AlgoSPN.getTerminatedSum()*1.0/AlgoSPN.getNbTerminated()));
						break;
					}
				}
			}
		);
		
		model = new DefaultTableModel(); 
		data_T = new JTable(model){ // disable editing the table by overriding the function
			// Serial version ID 
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		for (int i = 0; i < data_T_Columns.length; i++)
			model.addColumn(data_T_Columns[i]);
		JScrollPane scrollPane = new JScrollPane(data_T); // used to show the headers 'columns names'
		data_T.setFillsViewportHeight(true);
		data_T.getTableHeader().setReorderingAllowed(false); // disable the user from dragging columns
		scrollPane.setSize(data_TDim);
		scrollPane.setLocation(data_T_loc);
	
		super.add(scrollPane);
		super.add(algoAv);
		super.add(algoName);
		super.add(nbProcess);
		super.add(perfOutput_B);
		super.setSize(frameDim);
		super.setTitle("Performance");
		super.setLayout(null);
	}
	
	public static void init(){
		PerformanceOutput.model.getDataVector().removeAllElements();
		PerformanceOutput.model.fireTableDataChanged();
		
		algoName.setText("Algorithm : "+Control.algorithms[algoNb]);
		nbProcess.setText("nbProcess : ");
		algoAv.setText("average : ");
	}
}
