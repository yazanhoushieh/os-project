/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

public class Process {
	private int ID;	// Process ID to be known
	private int cpuTime1; // execution time in cpu, first round 
	private int cpuTime2; // execution time in cpu, second round - after seeing the io
	private int state;	// ready, running, new .... 
	private int mode; //to check if i am at cpu1 or cpu2  [1:cpu1, 2:cpu2]
 	private int taTimeSec; // hold the turn around time in seconds 
 	private int startTimeSec;// hold the start time in seconds 
	private IO io;
	private	Memory mem;
	private String startTime;
	private String terminationTime;
	//public static final int  newState = 0;
	public static final int runningState = 0;
	public static final int readyState = 1;
	public static final int readySuspendState  = 2;
	public static final int blockedState = 3;
	public static final int blockedSuspendState = 4;
	public static final int terminationState = 5;
	
	public static String[] stateHeader = {"Running", "Ready", "Ready-Suspend", "Blocked", "Blocked-Suspend", "Terminated"};
	
	public Process(Process p){
		this.setID(p.getID());
		this.setState(p.getState());
		this.setStartTime(p.getStartTime());
		this.setTerminationTime(p.getTerminationTime());
		this.setCpuTime1(p.getCpuTime1());
		this.setCpuTime2(p.getCpuTime2());
		this.setIo( new  IO(p.getIo().getTimeInIO(), p.getIo().getIoType()) );
		this.setMem(new Memory(p.getMem().getSize()));
		this.setMode(p.getMode());
		this.setStartTimeSec(p.getStartTimeSec());
		this.setTaTimeSec(p.getTaTimeSec());
 	}
	//  process but with a new state
	public Process(Process p, int state){
		this.setID(p.getID());
		this.setState(state);
		this.setStartTime(p.getStartTime());
		this.setTerminationTime(p.getTerminationTime());
		this.setCpuTime1(p.getCpuTime1());
		this.setCpuTime2(p.getCpuTime2());
		this.setIo( new  IO(p.getIo().getTimeInIO(), p.getIo().getIoType()) );
		this.setMem(new Memory(p.getMem().getSize()));
		this.setMode(p.getMode());
		this.setStartTimeSec(p.getStartTimeSec());
		this.setTaTimeSec(p.getTaTimeSec());
 	}
	// constructor
	public Process(int ID, int state, String startTime, int startTimeSec,String terminationTime, int cpuTime1, int cpuTime2,
			       int size, int timeInIO, String ioType, int mode){
		this.setID(ID);
		this.setState(state);
		this.setStartTime(startTime);
		this.setTerminationTime(terminationTime);
		this.setCpuTime1(cpuTime1);
		this.setCpuTime2(cpuTime2);
		this.setIo( new  IO(timeInIO, ioType) );
		this.setMem(new Memory(size));
		this.setMode(mode);
		this.setStartTimeSec(startTimeSec);
		this.setTaTimeSec(0);
	}
	 
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

	public void setCpuTime1(int cpuTime1) {
		this.cpuTime1 = cpuTime1;
	}
	public IO getIo() {
		return io;
	}
	public void setIo(IO io) {
		this.io = io;
	}
	public Memory getMem() {
		return mem;
	}
	public void setMem(Memory mem) {
		this.mem = mem;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getCpuTime1() {
		return cpuTime1;
	}
	
	public int getCpuTime2() {
		return cpuTime2;
	}
	public void setCpuTime2(int cpuTime2) {
		this.cpuTime2 = cpuTime2;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getTerminationTime() {
		return terminationTime;
	}

	public void setTerminationTime(String terminationTime) {
		this.terminationTime = terminationTime;
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
	}
	public int getTaTimeSec() {
		return taTimeSec;
	}
	public void setTaTimeSec(int taTimeSec) {
		this.taTimeSec = taTimeSec;
	}
	public int getStartTimeSec() {
		return startTimeSec;
	}
	public void setStartTimeSec(int startTimeSec) {
		this.startTimeSec = startTimeSec;
	}
}
