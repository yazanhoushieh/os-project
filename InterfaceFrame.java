/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

import java.awt.Color;
//import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.Timer;
import java.util.TimerTask;
//

public class InterfaceFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	// Timer
	private Timer timer;
	private TimerClass tc;
	public static int timerSecCnt = 0;
	public static int timerMinCnt = 0;
	public static int timerHourCnt = 0;
	// Dimensions 
	private Dimension frameDim;
	private Dimension compDim; // component size
	private Dimension data_TDim;
	// Locations 
	private Point cpu1Time_TF_loc;
	private Point cpu2Time_TF_loc;
	private Point ioTime_TF_loc;
	private Point dispatcher_TF_loc;
	private Point ioType_CB_loc;
	private Point algorithms_CB_loc;
	private Point insertProcess_B_loc;
	private Point perfOutput_B_loc;
	private Point maxMemSize_L_loc;
	private Point currMemSize_L_loc;
	private Point nbProcess_L_loc;
	private Point timer_L_loc;
	private Point ready_L_loc;
	private Point ready$suspend_L_loc;
	private Point blocked_L_loc;
	private Point blocked$suspend_L_loc;
	private Point running_L_loc;
	private Point terminated_L_loc;
	private Point data_T_loc;
	private Point timeSlice_PB_loc;
	private Point memUsage_PB_loc;
	
	// Components 
	private JTextField cpu1Time_TF;
	private JTextField cpu2Time_TF;
	private JTextField ioTime_TF; // to get the execution time of an io
	private JTextField dispatcher_TF;
	private JButton insertProcess_B;
	private JButton perfOutput_B;	// performance output for the algorithm
	private JComboBox<String> ioType_CB; // io combo box
	private JComboBox<String> algorithms_CB; // io combo box
	private JLabel maxMemSize_L;
	private JLabel currMemSize_L;
	private JLabel nbProcess_L;
	private JLabel timer_L;
	private JLabel ready_L;
	private JLabel ready$suspend_L;
	private JLabel blocked_L;
	private JLabel blocked$suspend_L;
	private JLabel running_L;
	private JLabel terminated_L;
	private static JTable data_T;
	private JProgressBar timeSlice_PB;
	private JProgressBar memUsage_PB;
	// table columns headers 
	public static DefaultTableModel model;
	private static String [] data_T_Columns = {"ID","State" , "Start Time", "Terminat T.",
												"cpuTime1", "timeInIO", "cpuTime2", "size" , "ioType", "Time Around"};
	private InterfaceFrame thisObj; // Reference to this object
	private Font myFont; // light gray, italic font
	private Font defaultFont; // black, plain font
	public int currTimeSlice; // dum var. to hold the dispatcher time 
	private int buttonEnable = 0; // boolean value to enable the JButton
	private PerformanceOutput perfOutputWindow;
	InterfaceFrame(){
		new PerformanceOutput();
		//TODO if the io time == 0 then disable the combo box of the io, else throw exception if not specified 
		perfOutputWindow = new PerformanceOutput();
		// set random value for the dispatcher between 1-6
		Control.dispatcherTime = (int)(Math.random()*6 + 1);
		currTimeSlice = Control.dispatcherTime;
		// make thisFrame point to the current object
		thisObj = this;
				
		// initiate the font 
		myFont = new Font("Time new Romans", Font.ITALIC, 12);
		defaultFont = new Font("Time new Romans", Font.PLAIN, 12);
		// Set the Dimensions of the Frame and Contents Dimensions and locations 
		frameDim = new Dimension(840, 600);
		compDim = new Dimension(90, 30);
		data_TDim = new Dimension(820, 200);
		cpu1Time_TF_loc = new Point(10, 10);
		ioTime_TF_loc = new Point(50, 10);
		cpu2Time_TF_loc = new Point(90, 10);
		dispatcher_TF_loc = new Point(10, 50);
		ioType_CB_loc = new Point(10, 90);
		algorithms_CB_loc = new Point(10, 130);
		insertProcess_B_loc = new Point(10, 210);
		perfOutput_B_loc = new Point(10, 250);
		maxMemSize_L_loc = new Point(200, 10);
		currMemSize_L_loc = new Point(200, 50);
		nbProcess_L_loc = new Point(200, 90);
		timer_L_loc = new Point(200, 130);
		ready_L_loc = new Point(400, 10);
		ready$suspend_L_loc = new Point(400, 50);
		blocked_L_loc = new Point(400, 90);
		blocked$suspend_L_loc = new Point(400, 130);
		running_L_loc = new Point(400, 170);
		data_T_loc = new Point(5, 350);
		timeSlice_PB_loc = new Point(5, 340);
		terminated_L_loc = new Point(400, 200);
		memUsage_PB_loc = new Point(200, 80);
		
		// Set the content components
		/*Tricky case : it won't show the Header unless you add them to JScroll and if not u need to place them 
		 * by your self check out http://docs.oracle.com/javase/tutorial/uiswing/components/table.html#simple*/
		model = new DefaultTableModel(); 
		data_T = new JTable(model){ // disable editing the table by overriding the function
			// Serial version ID 
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		for (int i = 0; i < data_T_Columns.length; i++)
			model.addColumn(data_T_Columns[i]);
		JScrollPane scrollPane = new JScrollPane(data_T); // used to show the headers 'columns names'
		data_T.setFillsViewportHeight(true);
		data_T.getTableHeader().setReorderingAllowed(false); // disable the user from dragging columns
		scrollPane.setSize(data_TDim);
		scrollPane.setLocation(data_T_loc);
		
		cpu1Time_TF = new HintTextField("CPU 1");
		cpu1Time_TF.setSize((int)compDim.getWidth()-50, (int)compDim.getHeight());
		cpu1Time_TF.setLocation(cpu1Time_TF_loc);
		cpu1Time_TF.setFont(myFont);
		cpu1Time_TF.setForeground(Color.LIGHT_GRAY);
		cpu1Time_TF.addKeyListener(new KeyListenerHandler());
		
		cpu2Time_TF = new HintTextField("CPU 2");
		cpu2Time_TF.setSize((int)compDim.getWidth()-50, (int)compDim.getHeight());
		cpu2Time_TF.setLocation(cpu2Time_TF_loc);
		cpu2Time_TF.setFont(myFont);
		cpu2Time_TF.setForeground(Color.LIGHT_GRAY);
		cpu2Time_TF.addKeyListener(new KeyListenerHandler());
		
		ioTime_TF = new HintTextField("IO");
		ioTime_TF.setSize((int)compDim.getWidth()-50, (int)compDim.getHeight());
		ioTime_TF.setLocation(ioTime_TF_loc);
		ioTime_TF.setFont(myFont);
		ioTime_TF.setForeground(Color.LIGHT_GRAY);
		ioTime_TF.addKeyListener(new KeyListenerHandler());
		
		dispatcher_TF = new HintTextField("Dispatch Time");
		dispatcher_TF.setSize((int)compDim.getWidth(), (int)compDim.getHeight());
		dispatcher_TF.setLocation(dispatcher_TF_loc);
		dispatcher_TF.setFont(myFont);
		dispatcher_TF.setForeground(Color.LIGHT_GRAY);
		dispatcher_TF.addKeyListener(new KeyListenerHandler());
		
		ioType_CB = new JComboBox<String>(IO.getIodevices());
		ioType_CB.setSize(compDim);
		ioType_CB.setLocation(ioType_CB_loc);
		
		algorithms_CB = new JComboBox<String>(Control.getAlgorithms());
		algorithms_CB.setSize((int)compDim.getWidth()+30, (int)compDim.getHeight());
		algorithms_CB.setLocation(algorithms_CB_loc);
		
		maxMemSize_L = new JLabel(String.format("Max.Mem. %dKB", Memory.getMaxMemorySize()));
		maxMemSize_L.setLocation(maxMemSize_L_loc);
		maxMemSize_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		maxMemSize_L.setForeground(Color.blue);
		
		currMemSize_L = new JLabel(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
		currMemSize_L.setLocation(currMemSize_L_loc);
		currMemSize_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		currMemSize_L.setForeground(Color.blue);
		
		nbProcess_L = new JLabel(String.format("Nb. in Processesing : 0"));
		nbProcess_L.setLocation(nbProcess_L_loc);
		nbProcess_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		nbProcess_L.setForeground(Color.blue);
		
		terminated_L = new JLabel(String.format("Nb. of Terminated: 0"));
		terminated_L.setLocation(terminated_L_loc);
		terminated_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight()+5);
		terminated_L.setForeground(Color.blue);
		
		timer_L = new JLabel(String.format("Time : 00:00:00"));
		timer_L.setLocation(timer_L_loc);
		timer_L.setSize((int)compDim.getWidth()+30, (int)compDim.getHeight());
		timer_L.setForeground(Color.blue);
		
		ready_L = new JLabel(String.format("Nb. Ready : %d", 0));
		ready_L.setLocation(ready_L_loc);
		ready_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		ready_L.setForeground(Color.blue);
		
		ready$suspend_L = new JLabel(String.format("Nb. R/S : %d", 0));
		ready$suspend_L.setLocation(ready$suspend_L_loc);
		ready$suspend_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		ready$suspend_L.setForeground(Color.blue);
		
		blocked_L = new JLabel(String.format("Nb. Blocked : %d", 0));
		blocked_L.setLocation(blocked_L_loc);
		blocked_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		blocked_L.setForeground(Color.blue);
		
		blocked$suspend_L = new JLabel(String.format("Nb. B/S : %d", 0));
		blocked$suspend_L.setLocation(blocked$suspend_L_loc);
		blocked$suspend_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		blocked$suspend_L.setForeground(Color.blue);
		
		running_L = new JLabel("P.(ID=NIL) Running");
		running_L.setLocation(running_L_loc);
		running_L.setSize((int)compDim.getWidth() + 90, (int)compDim.getHeight());
		running_L.setForeground(Color.blue);
		
		insertProcess_B = new JButton("Insert P");
		insertProcess_B.setSize(compDim);
		insertProcess_B.setLocation(insertProcess_B_loc);
		insertProcess_B.addMouseListener(
				new MouseAdapter() {
					public void mouseEntered(MouseEvent e) {
						insertProcess_B.setBackground(Color.white);
					}
					public void mouseExited(MouseEvent e) {
						insertProcess_B.setBackground(thisObj.getBackground());
					}
				}
		);
		insertProcess_B.addActionListener(
				new ActionListener() {	
				//	@Override
					public void actionPerformed(ActionEvent e) {
						try{
							algorithms_CB.setEnabled(false);
							perfOutput_B.setEnabled(false);
							// [Generate a Nb between Min and Max][Min + Math.rand()*Max]
							int ID = 0/*Min*/ + (int)(Math.random()*30001/*Max*/);
							while(Control.isFound(ID))
								ID = 0/*Min*/ + (int)(Math.random()*30001/*Max*/);
 							int cpuTime1 = Integer.parseInt(cpu1Time_TF.getText());
 							int cpuTime2 = Integer.parseInt(cpu2Time_TF.getText());
							int timeInIO = Integer.parseInt(ioTime_TF.getText());
							String ioType;
							if (timeInIO == 0)
								ioType = ioType_CB.getItemAt(0);// set to no-io 
							else
								ioType = ioType_CB.getItemAt(ioType_CB.getSelectedIndex());
							
							int size  =  300/*Min*/ + (int)(Math.random()*1000/*Max*/); 								
							int state;
							String startTime = String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt);// extract time  of start
							int startTimeSec = timerSecCnt + (timerMinCnt*60) + (timerHourCnt*3600);
							String terminationTime = -1+"";
							/*When i want to add a new process i have a choice of two paths :1. Ready[if there is 
							 * memory] and 2. Ready-suspend if there is not*/
							if (size > (Memory.getMaxMemorySize()-Memory.getCurrentSize() )){ // Add to the Ready 
								state = Process.readySuspendState;
							}
							else{ // Add to the ready  
								state = Process.readyState;
								Memory.setCurrentSize(size);
							}
							// mode = 1, indicates cpu1 at the beginning 
							switch(algorithms_CB.getSelectedIndex()){
							case 0:
								AlgoRoundRobin.addProcess(ID, state, startTime, startTimeSec, terminationTime, cpuTime1, cpuTime2, size, timeInIO, ioType, 1);
								nbProcess_L.setText(String.format("# in Processesing : %d", AlgoRoundRobin.getTotalNbProcess()));
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								ready_L.setText(String.format("Nb. Ready : %d", AlgoRoundRobin.getNbReady()));
								ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoRoundRobin.getNbReady$suspend()));
								break;
							case 1:
								AlgoFCFS.addProcess(ID, state, startTime, startTimeSec, terminationTime, cpuTime1, cpuTime2, size, timeInIO, ioType, 1);
								nbProcess_L.setText(String.format("# in Processesing : %d", AlgoFCFS.getTotalNbProcess()));
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								ready_L.setText(String.format("Nb. Ready : %d", AlgoFCFS.getTotalNbProcess()));
								ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoFCFS.getNbReady$suspend()));
								break;
							case 2:
								AlgoSPN.addProcess(ID, state, startTime, startTimeSec, terminationTime, cpuTime1, cpuTime2, size, timeInIO, ioType, 1);
								nbProcess_L.setText(String.format("# in Processesing : %d", AlgoSPN.getTotalNbProcess()));
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								ready_L.setText(String.format("Nb. Ready : %d", AlgoSPN.getTotalNbProcess()));
								ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoSPN.getNbReady$suspend()));
								break;
							}
							model.addRow(new Object[]{ID, Process.stateHeader[state], startTime, terminationTime, 
					                  cpuTime1, timeInIO, cpuTime2, size, ioType, "--:--:--"});
							data_T.requestFocus();
							data_T.changeSelection(data_T.getRowCount()-1, 0,false, false);
							insertProcess_B.setEnabled(false); // give it a second to draw 
							buttonEnable = 2;
						}
						catch(NumberFormatException excep){
							JOptionPane.showMessageDialog(thisObj, "Unvalid or Incorrect Process input", "Input Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				
		);
		
		perfOutput_B = new JButton("Show Performance");
		perfOutput_B.setSize((int)compDim.getWidth()+90, (int)compDim.getHeight());
		perfOutput_B.setLocation(perfOutput_B_loc);
		perfOutput_B.addMouseListener(
				new MouseAdapter() {
					public void mouseEntered(MouseEvent e) {
						perfOutput_B.setBackground(Color.white);
					}
					public void mouseExited(MouseEvent e) {
						perfOutput_B.setBackground(thisObj.getBackground());
					}
				}
		);
		perfOutput_B.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PerformanceOutput.algoNb = algorithms_CB.getSelectedIndex();
					PerformanceOutput.init();
					perfOutputWindow.setVisible(true);
				}
			}
		);
		
		timeSlice_PB = new JProgressBar();
		timeSlice_PB.setLocation(timeSlice_PB_loc);
		timeSlice_PB.setSize(820, 10);
		timeSlice_PB.setForeground(Color.YELLOW);
		
		memUsage_PB = new JProgressBar();
		memUsage_PB.setLocation(memUsage_PB_loc);
		memUsage_PB.setSize(100, 10);
		memUsage_PB.setForeground(Color.YELLOW);
		
		
		// initiate timer
		tc = new TimerClass();
		timer = new Timer();
		timer.schedule(tc, 1000, 1000);
				
		// add the contents to the frame
		//super.add(data_T);
		super.add(scrollPane);
		super.add(cpu1Time_TF);
		super.add(cpu2Time_TF);
		super.add(ioTime_TF);
		super.add(dispatcher_TF);
		super.add(ioType_CB);
		super.add(algorithms_CB);
		super.add(insertProcess_B);
		super.add(perfOutput_B);
		super.add(maxMemSize_L);
		super.add(currMemSize_L);
		super.add(nbProcess_L);
		super.add(timer_L);
		super.add(ready_L);
		super.add(ready$suspend_L);
		super.add(blocked_L);
		super.add(blocked$suspend_L);
		super.add(running_L);
		super.add(terminated_L);
		super.add(timeSlice_PB);
		super.add(memUsage_PB);
		super.setFocusable(true); // So that it don't focus on the first text field 
		// set the frame properties  
		super.setLocationRelativeTo(null); // set in the middle of the screen
		//Container c = getContentPane();
		//c.setBackground(Color.white);
		super.setTitle("OS_Project");
		super.setLayout(null);
		super.setSize(frameDim);
		super.setResizable(false);
		super.setVisible(true);
	}// End of constructor 
	
	// TODO delete if not necessary 
	public class ActionListenerHandler extends MouseAdapter{
		public void mouseClicked(MouseEvent e) {
			if (e.getSource() == cpu1Time_TF){
				cpu1Time_TF.setText("");
				cpu1Time_TF.setFont(defaultFont);
				cpu1Time_TF.setForeground(Color.BLACK);
			}
			if (e.getSource() == cpu2Time_TF){
				cpu2Time_TF.setText("");
				cpu2Time_TF.setFont(defaultFont);
				cpu2Time_TF.setForeground(Color.BLACK);
			}
			if (e.getSource() == ioTime_TF){
				ioTime_TF.setText("");
				ioTime_TF.setFont(defaultFont);
				ioTime_TF.setForeground(Color.BLACK);
			}
			if (e.getSource() == dispatcher_TF){
				dispatcher_TF.setText("");
				dispatcher_TF.setFont(defaultFont);
				dispatcher_TF.setForeground(Color.BLACK);
			}
		}
	} // End of ActionListenerHandler class
	
	public class KeyListenerHandler extends KeyAdapter{
		public void keyTyped(KeyEvent e){
			char c = e.getKeyChar();
			if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)))
				e.consume(); // consume so that it's not processed 
			if (e.getSource() == cpu1Time_TF){
				cpu1Time_TF.setFont(defaultFont);
				cpu1Time_TF.setForeground(Color.black);
			}
			else
				if (e.getSource() == cpu2Time_TF){
					cpu2Time_TF.setFont(defaultFont);
					cpu2Time_TF.setForeground(Color.black);
				}
				else
					if (e.getSource() == ioTime_TF){
						ioTime_TF.setFont(defaultFont);
						ioTime_TF.setForeground(Color.black);
					}
					else
					if (e.getSource() == dispatcher_TF){
						dispatcher_TF.setFont(defaultFont);
						dispatcher_TF.setForeground(Color.black);
					}
					
		}
	}// End of KeyListenerHandler class 
	
	public class TimerClass extends TimerTask{
		public void run(){
 			++timerSecCnt;
			if (timerSecCnt == 60){
				timerSecCnt = 0;
				timerMinCnt++;
			}
			if (timerMinCnt == 60){
				timerMinCnt = 0;
				timerHourCnt++;
			}
			if (timerHourCnt == 24){
				timerSecCnt = 0;
				timerMinCnt = 0;
				timerHourCnt = 0;
			}
			if (buttonEnable-- == 0){
				insertProcess_B.setEnabled(true);
			}
			timer_L.setText("Time : " + String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt));
			switch(algorithms_CB.getSelectedIndex()){
			case 0://AlgoRoundRobin
				dispatcher_TF.setEnabled(true);
				currTimeSlice--;
				if (AlgoRoundRobin.getTotalNbProcess() != 0){
					AlgoRoundRobin.decrementIO(); // decrement io time for B and B/S
					
					if (AlgoRoundRobin.getNbRunning() == 1){
						AlgoRoundRobin.decrementRunning(AlgoRoundRobin.getCurrentP().getMode());
					}
					else{ // Running == NIL
						// I need to check if Ready has element , If not then i have to wait
						if (AlgoRoundRobin.getNbReady() != 0){
							AlgoRoundRobin.assignRunning();// assign process to running from the queue 
							currTimeSlice = Control.dispatcherTime;
							AlgoRoundRobin.getCurrentP().setState(Process.runningState);
						}
					}
					
					if (currTimeSlice == 0 && AlgoRoundRobin.getNbRunning() == 1){
						if (AlgoRoundRobin.getCurrentP().getMode() == 1){
							if  (AlgoRoundRobin.getCurrentP().getCpuTime1() == 0){
								AlgoRoundRobin.getCurrentP().setMode(2);
								if (AlgoRoundRobin.getCurrentP().getIo().getTimeInIO() == 0){
									// insert back to ready 
									AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.readyState);
									AlgoRoundRobin.clearRunning();
								}
								else{ // IO != 0
									// insert into Blocked 
									AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.blockedState);
									AlgoRoundRobin.clearRunning();
								}
							}
							else{ // getCpuTime1 != 0
								AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.readyState);
								AlgoRoundRobin.clearRunning();
							}
						} // End of Control.getCurrentP().getMode() == 1
						else{ // MODE is either 1 or 2 
							if  (AlgoRoundRobin.getCurrentP().getCpuTime2() == 0){
								AlgoRoundRobin.getCurrentP().setTerminationTime(String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt));
								//setTaTimeSec , startTime-currentTime
								AlgoRoundRobin.getCurrentP().setTaTimeSec(Math.abs((timerSecCnt + (timerMinCnt*60) + (timerHourCnt*3600))-
										AlgoRoundRobin.getCurrentP().getStartTimeSec()));
								AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.terminationState);
							
								Memory.setCurrentSize(-AlgoRoundRobin.getCurrentP().getMem().getSize());
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								// add to the table again 
								AlgoRoundRobin.clearRunning();
							}
							else{ // getCpuTime1 != 0
								AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.readyState);
								AlgoRoundRobin.clearRunning();
							}
						}
					} // End of currTimeSlice==0 IF statement
					else
					if(AlgoRoundRobin.getNbRunning() == 1 && AlgoRoundRobin.getCurrentP().getMode() == 1 && AlgoRoundRobin.getCurrentP().getCpuTime1() == 0){
						AlgoRoundRobin.getCurrentP().setMode(2);
						if (AlgoRoundRobin.getCurrentP().getIo().getTimeInIO() == 0){
							// insert back to ready 
							AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.readyState);
							AlgoRoundRobin.clearRunning();
						}
						else{ // IO != 0
							// insert into Blocked 
							AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.blockedState);
							AlgoRoundRobin.clearRunning();
						}
					}
					else
						if(AlgoRoundRobin.getNbRunning() == 1 && AlgoRoundRobin.getCurrentP().getMode() == 2 && AlgoRoundRobin.getCurrentP().getCpuTime2() == 0){
							if  (AlgoRoundRobin.getCurrentP().getCpuTime2() == 0){
								Memory.setCurrentSize(-AlgoRoundRobin.getCurrentP().getMem().getSize());
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								
								AlgoRoundRobin.getCurrentP().setTerminationTime(String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt));
								AlgoRoundRobin.getCurrentP().setTaTimeSec(Math.abs((timerSecCnt + (timerMinCnt*60) + (timerHourCnt*3600))-
										AlgoRoundRobin.getCurrentP().getStartTimeSec()));
								AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.terminationState);
								AlgoRoundRobin.clearRunning();
							}
							else{ // getCpuTime1 != 0
								AlgoRoundRobin.addProcess(AlgoRoundRobin.getCurrentP(), Process.readyState);
								AlgoRoundRobin.clearRunning();
							}
						}

					// From Ready-Suspend --->   Ready
					if (AlgoRoundRobin.getNbReady$suspend() != 0){
						AlgoRoundRobin.RStoR();
					}
					// From Blocked ---> Blocked-Suspend
					if (AlgoRoundRobin.getNbReady$suspend() != 0 && AlgoRoundRobin.getNbReady() == 0 && AlgoRoundRobin.getNbBlocked() != 0){
						AlgoRoundRobin.BtoBS();
					}
					// From Blocked-Suspend ---> Blocked
					if (AlgoRoundRobin.getNbReady$suspend() == 0 && AlgoRoundRobin.getNbReady() == 0){
						//JOptionPane.showMessageDialog(null, "ha");
						AlgoRoundRobin.BStoB();
					}
					// From Blocked ---> Ready
					if (AlgoRoundRobin.getNbBlocked() != 0){
						AlgoRoundRobin.BtoR();
					}
					
					// Update the labels 
					running_L.setText(String.format("P.(ID=%s) Running", (AlgoRoundRobin.getNbRunning() == 0)?"NIL":AlgoRoundRobin.getCurrentP().getID()+""));
					blocked$suspend_L.setText(String.format("Nb. B/S : %d", AlgoRoundRobin.getNbBlocked$suspend()));
					blocked_L.setText(String.format("Nb. Blocked : %d", AlgoRoundRobin.getNbBlocked()));
					ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoRoundRobin.getNbReady$suspend()));
					ready_L.setText(String.format("Nb. Ready : %d", AlgoRoundRobin.getNbReady()));
					nbProcess_L.setText(String.format("Total Nb. Processes : %d", AlgoRoundRobin.getTotalNbProcess()));
					
					// Update the data_Tabel
					AlgoRoundRobin.fillTabel();
					// Update the progress bars 
					timeSlice_PB.setValue((int)(((Control.dispatcherTime-currTimeSlice)*1.0/Control.dispatcherTime)*100));
					memUsage_PB.setValue((int)((Memory.getCurrentSize()*1.0/Memory.getMaxMemorySize()*100)));
				}//End of Control.getTotalNbProcess() != 0 if statement
				else{
					algorithms_CB.setEnabled(true);
					perfOutput_B.setEnabled(true);
					timeSlice_PB.setValue(0);
				}
				terminated_L.setText(String.format("Nb. Terminated: %d", AlgoRoundRobin.getNbTerminated()));
				break;
			/************************************************************************************************/	
			case 1://AlgoFCFS
				dispatcher_TF.setEnabled(false);

				if (AlgoFCFS.getTotalNbProcess() != 0){
					dispatcher_TF.setEnabled(false);
					AlgoFCFS.decrementIO(); // decrement io time for B and B/S
					
					if (AlgoFCFS.getNbRunning() == 1){
						AlgoFCFS.decrementRunning(AlgoFCFS.getCurrentP().getMode());
					}
					else{ // Running == NIL
						// I need to check if Ready has element , If not then i have to wait
						if (AlgoFCFS.getNbReady() != 0){
							AlgoFCFS.assignRunning();// assign process to running from the queue 
							AlgoFCFS.getCurrentP().setState(Process.runningState);
						}
					}
					
					if(AlgoFCFS.getNbRunning() == 1 && AlgoFCFS.getCurrentP().getMode() == 1 && AlgoFCFS.getCurrentP().getCpuTime1() == 0){
						AlgoFCFS.getCurrentP().setMode(2);
						if (AlgoFCFS.getCurrentP().getIo().getTimeInIO() == 0){
							// insert back to ready 
							AlgoFCFS.addProcess(AlgoFCFS.getCurrentP(), Process.readyState);
							AlgoFCFS.clearRunning();
						}
						else{ // IO != 0
							// insert into Blocked 
							AlgoFCFS.addProcess(AlgoFCFS.getCurrentP(), Process.blockedState);
							AlgoFCFS.clearRunning();
						}
					}
					else
						if(AlgoFCFS.getNbRunning() == 1 && AlgoFCFS.getCurrentP().getMode() == 2 && AlgoFCFS.getCurrentP().getCpuTime2() == 0){
							if  (AlgoFCFS.getCurrentP().getCpuTime2() == 0){
								Memory.setCurrentSize(-AlgoFCFS.getCurrentP().getMem().getSize());
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								
								AlgoFCFS.getCurrentP().setTerminationTime(String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt));
								AlgoFCFS.getCurrentP().setTaTimeSec(Math.abs((timerSecCnt + (timerMinCnt*60) + (timerHourCnt*3600))-
										AlgoFCFS.getCurrentP().getStartTimeSec()));
								AlgoFCFS.addProcess(AlgoFCFS.getCurrentP(), Process.terminationState);
								AlgoFCFS.clearRunning();
							}
							else{ // getCpuTime1 != 0
								AlgoFCFS.addProcess(AlgoFCFS.getCurrentP(), Process.readyState);
								AlgoFCFS.clearRunning();
							}
						}

					// From Ready-Suspend --->   Ready
					if (AlgoFCFS.getNbReady$suspend() != 0){
						AlgoFCFS.RStoR();
					}
					// From Blocked ---> Blocked-Suspend
					if (AlgoFCFS.getNbReady$suspend() != 0 && AlgoFCFS.getNbReady() == 0 && AlgoFCFS.getNbBlocked() != 0){
						AlgoFCFS.BtoBS();
					}
					// From Blocked-Suspend ---> Blocked
					if (AlgoFCFS.getNbReady$suspend() == 0 && AlgoFCFS.getNbReady() == 0){
						//JOptionPane.showMessageDialog(null, "ha");
						AlgoFCFS.BStoB();
					}
					// From Blocked ---> Ready
					if (AlgoFCFS.getNbBlocked() != 0){
						AlgoFCFS.BtoR();
					}
					
					// Update the labels 
					running_L.setText(String.format("P.(ID=%s) Running", (AlgoFCFS.getNbRunning() == 0)?"NIL":AlgoFCFS.getCurrentP().getID()+""));
					blocked$suspend_L.setText(String.format("Nb. B/S : %d", AlgoFCFS.getNbBlocked$suspend()));
					blocked_L.setText(String.format("Nb. Blocked : %d", AlgoFCFS.getNbBlocked()));
					ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoFCFS.getNbReady$suspend()));
					ready_L.setText(String.format("Nb. Ready : %d", AlgoFCFS.getNbReady()));
					nbProcess_L.setText(String.format("Total Nb. Processes : %d", AlgoFCFS.getTotalNbProcess()));
					
					// Update the data_Tabel
					AlgoFCFS.fillTabel();
					// Update the progress bars 
					if (AlgoFCFS.getTotalNbProcess() != 0)
						timeSlice_PB.setValue((AlgoFCFS.getNbTerminated()/AlgoFCFS.getTotalNbProcess())*100);
					else 
						timeSlice_PB.setValue(0);
					memUsage_PB.setValue((int)((Memory.getCurrentSize()*1.0/Memory.getMaxMemorySize()*100)));
				}//End of Control.getTotalNbProcess() != 0 if statement
				else{
					algorithms_CB.setEnabled(true);
					perfOutput_B.setEnabled(true);
					timeSlice_PB.setValue(0);
					dispatcher_TF.setEnabled(true);
				}
				terminated_L.setText(String.format("Nb. Terminated: %d", AlgoFCFS.getNbTerminated()));
				break;
				/************************************************************************************************/	
			case 2://AlgoSPN
				if (AlgoSPN.getTotalNbProcess() != 0){
					dispatcher_TF.setEnabled(false);
					AlgoSPN.decrementIO(); // decrement io time for B and B/S
					
					if (AlgoSPN.getNbRunning() == 1){
						AlgoSPN.decrementRunning(AlgoSPN.getCurrentP().getMode());
					}
					else{ // Running == NIL
						// I need to check if Ready has element , If not then i have to wait
						if (AlgoSPN.getNbReady() != 0){
							AlgoSPN.assignRunning();// assign process to running from the queue 
							AlgoSPN.getCurrentP().setState(Process.runningState);
						}
					}
					
					if(AlgoSPN.getNbRunning() == 1 && AlgoSPN.getCurrentP().getMode() == 1 && AlgoSPN.getCurrentP().getCpuTime1() == 0){
						AlgoSPN.getCurrentP().setMode(2);
						if (AlgoSPN.getCurrentP().getIo().getTimeInIO() == 0){
							// insert back to ready 
							AlgoSPN.addProcess(AlgoSPN.getCurrentP(), Process.readyState);
							AlgoSPN.clearRunning();
						}
						else{ // IO != 0
							// insert into Blocked 
							AlgoSPN.addProcess(AlgoSPN.getCurrentP(), Process.blockedState);
							AlgoSPN.clearRunning();
						}
					}
					else
						if(AlgoSPN.getNbRunning() == 1 && AlgoSPN.getCurrentP().getMode() == 2 && AlgoSPN.getCurrentP().getCpuTime2() == 0){
							if  (AlgoSPN.getCurrentP().getCpuTime2() == 0){
								Memory.setCurrentSize(-AlgoSPN.getCurrentP().getMem().getSize());
								currMemSize_L.setText(String.format("Mem. Usage %dKB", Memory.getCurrentSize()));
								
								AlgoSPN.getCurrentP().setTerminationTime(String.format("%02d:%02d:%02d", timerHourCnt, timerMinCnt, timerSecCnt));
								AlgoSPN.getCurrentP().setTaTimeSec(Math.abs((timerSecCnt + (timerMinCnt*60) + (timerHourCnt*3600))-
										AlgoSPN.getCurrentP().getStartTimeSec()));
								AlgoSPN.addProcess(AlgoSPN.getCurrentP(), Process.terminationState);
								AlgoSPN.clearRunning();
							}
							else{ // getCpuTime1 != 0
								AlgoSPN.addProcess(AlgoSPN.getCurrentP(), Process.readyState);
								AlgoSPN.clearRunning();
							}
						}

					// From Ready-Suspend --->   Ready
					if (AlgoSPN.getNbReady$suspend() != 0){
						AlgoSPN.RStoR();
					}
					// From Blocked ---> Blocked-Suspend
					if (AlgoSPN.getNbReady$suspend() != 0 && AlgoSPN.getNbReady() == 0 && AlgoSPN.getNbBlocked() != 0){
						AlgoSPN.BtoBS();
					}
					// From Blocked-Suspend ---> Blocked
					if (AlgoSPN.getNbReady$suspend() == 0 && AlgoSPN.getNbReady() == 0){
						//JOptionPane.showMessageDialog(null, "ha");
						AlgoSPN.BStoB();
					}
					// From Blocked ---> Ready
					if (AlgoSPN.getNbBlocked() != 0){
						AlgoSPN.BtoR();
					}
					
					// Update the labels 
					running_L.setText(String.format("P.(ID=%s) Running", (AlgoSPN.getNbRunning() == 0)?"NIL":AlgoSPN.getCurrentP().getID()+""));
					blocked$suspend_L.setText(String.format("Nb. B/S : %d", AlgoSPN.getNbBlocked$suspend()));
					blocked_L.setText(String.format("Nb. Blocked : %d", AlgoSPN.getNbBlocked()));
					ready$suspend_L.setText(String.format("Nb. R/S : %d", AlgoSPN.getNbReady$suspend()));
					ready_L.setText(String.format("Nb. Ready : %d", AlgoSPN.getNbReady()));
					nbProcess_L.setText(String.format("Total Nb. Processes : %d", AlgoSPN.getTotalNbProcess()));
					
					// Update the data_Tabel
					AlgoSPN.fillTabel();
					// Update the progress bars 
					if (AlgoSPN.getTotalNbProcess() != 0)
						timeSlice_PB.setValue((AlgoSPN.getNbTerminated()/AlgoSPN.getTotalNbProcess())*100);
					else 
						timeSlice_PB.setValue(0);
					memUsage_PB.setValue((int)((Memory.getCurrentSize()*1.0/Memory.getMaxMemorySize()*100)));
				}//End of Control.getTotalNbProcess() != 0 if statement
				else{
					algorithms_CB.setEnabled(true);
					perfOutput_B.setEnabled(true);
					timeSlice_PB.setValue(0);
					dispatcher_TF.setEnabled(true);
				}
				terminated_L.setText(String.format("Nb. Terminated: %d", AlgoSPN.getNbTerminated()));
				break;
			}
			// Now i need to make the proper checks and change the state of the different processes 
			
		}// End of run
	}// End of TimerClass
	
	class HintTextField extends JTextField implements FocusListener {
		private static final long serialVersionUID = 1L;
		
		private final String hint;
		private boolean showingHint;

		
		public HintTextField(final String hint) {
			super(hint);
			this.hint = hint;
		    this.showingHint = true;
		    super.addFocusListener(this);
		  }

		  //Invoked when a component gains the keyboard focus.
		 // @Override
		  public void focusGained(FocusEvent e) {
		    if(this.getText().isEmpty()) {
		      super.setText("");
		      showingHint = false;
		    }
		  }
		  //Invoked when a component lose the keyboard focus.
		//  @Override
		  public void focusLost(FocusEvent e) {
		    if(this.getText().isEmpty()) {
		      super.setFont(myFont);
		      super.setForeground(Color.lightGray);
		      super.setText(hint);
		      showingHint = true;
		    }
		  }

		  @Override
		  public String getText() {
		    return showingHint ? "" : super.getText();
		  }
		}// End of Hint listener
}// End of InterfaceFrame class 