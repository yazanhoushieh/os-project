/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

import java.util.HashMap;
import java.util.Map;

public class Control {
	private static Map<Integer, Boolean> idArray  = new HashMap<Integer, Boolean>(); // this will hold the current available id's 
	public static final String [] algorithms = {"Round Robin", "FCFS", "SPN"};
		
	public static String[] getAlgorithms() {
		return algorithms;
	}
	// and make sure that you generate id  that is not already taken
	public static int dispatcherTime;
	/*Find if a specific ID exist [to avoid duplication of ID's]*/
	public static boolean isFound(Integer ID){
		if(idArray.containsKey(ID))
			return true;
		return false;
	}
	
	// return string format of the turn around time of the process 
	public static String TATime(long r){
		long result = r;
		String ttTime = String.format("%02d:",(result/3600));
		result %= 3600;
		ttTime += String.format("%02d:",(result/60));
		result %= 60;
		ttTime += String.format("%02d",result);
		
		return ttTime;
	}
}//End of Control