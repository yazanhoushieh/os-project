package os_project;

public class IOTable {
	private int timeInIO; // time needed in the IO if exist 
	private String ioType;
	private static final String [] ioDevices = {"No - IO", "Mouse", "Printer", "Keyboard", "Screen"};
	
	public static String[] getIodevices() {
		return ioDevices;
	}
	public IOTable(int timeInIO, String ioType){
		this.setTimeInIO(timeInIO);
		this.setIoType(ioType);
	}
	public String getIoType() {
		return ioType;
	}
	public void setIoType(String ioType) {
		this.ioType = ioType;
	}
	public int getTimeInIO() {
		return timeInIO;
	}
	public void setTimeInIO(int timeInIO) {
		this.timeInIO = timeInIO;
	}
	public String toString(){
		return (String.format("timeInIO = %d\nioType = %d\n", this.getTimeInIO(), this.getIoType()));
	}
}
