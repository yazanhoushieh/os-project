/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

import javax.swing.JFrame;

public class MainDriver {	
	public static void main(String argv[]){
		InterfaceFrame application = new InterfaceFrame();
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}	
}
