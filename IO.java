/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* | 															   | */
/* | 															   | */
/* | 				Operating Systems Project					   | */
/* | 							2014							   | */
/* | 			@author Yazan A. Houshieh (#110181)				   | */
/* | 															   | */
/* | 															   | */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package os_project;

public class IO {
	private int timeInIO; // time needed in the IO if exist 
	private String ioType;
	private static final String [] ioDevices = {"No - IO", "Mouse", "Printer", "Keyboard", "Screen"};
	
	
	public IO(int timeInIO, String ioType){
		this.setTimeInIO(timeInIO);
		this.setIoType(ioType);
	}
	public static String[] getIodevices() {
		return ioDevices;
	}
	public String getIoType() {
		return ioType;
	}
	public void setIoType(String ioType) {
		this.ioType = ioType;
	}
	public int getTimeInIO() {
		return timeInIO;
	}
	public void setTimeInIO(int timeInIO) {
		this.timeInIO = timeInIO;
	}
}
