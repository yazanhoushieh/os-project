package os_project;

public class ProcessTable {
	private int ID;	// Process ID to be known
	private int cpuTime1; // execution time in cpu, first round 
	private int cpuTime2; // execution time in cpu, second round - after seeing the io
	private int state;	// ready, running, new .... 
	private IOTable ioTabel;
	private	MemoryTabel memTabel;

	public static final int  newState = 0;
	public static final int readyState = 1;
	public static final int readySuspendState  = 2;
	public static final int blockedState = 3;
	public static final int blockedSuspendState = 4;
	public static final int exitState = 5;
	
	public static String[] stateHeader = {"New", "Ready", "Ready-Suspend", "Blocked", "Blocked-Suspend", "Exit"};
	
	
	// constructor
	public ProcessTable(int ID, int state, int cpuTime, int cpuTime2, int size, int timeInIO, String ioType){
		this.setID(ID);
		this.state = state;
		this.setCpuTime(cpuTime2);
		this.setCpuTime2(cpuTime2);
		this.setIoTabel( new  IOTable(timeInIO, ioType) );
		this.setMemTabel(new MemoryTabel(size));
	}
	
	// get info about this process 
	public String toString(){
		return (this.getID()+"\n"+stateHeader[this.getState()]+"\n"+this.getCpuTime1()+"\n"+this.getCpuTime2()+
				"\n" + this.getIoTabel().toString() + "\n" + this.getMemTabel().toString());
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

	public void setCpuTime1(int cpuTime1) {
		this.cpuTime1 = cpuTime1;
	}
	public IOTable getIoTabel() {
		return ioTabel;
	}
	public void setIoTabel(IOTable ioTabel) {
		this.ioTabel = ioTabel;
	}
	public MemoryTabel getMemTabel() {
		return memTabel;
	}
	public void setMemTabel(MemoryTabel memTabel) {
		this.memTabel = memTabel;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getCpuTime1() {
		return cpuTime1;
	}
	public void setCpuTime(int cpuTime) {
		this.cpuTime1 = cpuTime;
	}
	public int getCpuTime2() {
		return cpuTime2;
	}
	public void setCpuTime2(int cpuTime2) {
		this.cpuTime2 = cpuTime2;
	}
}
